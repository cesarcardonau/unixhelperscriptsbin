############################################################################
#Build a SQL realtional database                                          ##
#The database is created in SQlite3                                       ##
############################################################################
#! /bin/sh
function query_num_overlap {
		table1=$1
		table2=$2
                echo "select count(distinct bin_scaffold_gene) from $table1;" > db.query_num_hits.1
                sqlite3 proteintracker.db < db.query_num_hits.1 > db.query_num_hits.out.1
                echo "select count(distinct bin_scaffold_gene) from $table2;" > db.query_num_hits.2
                sqlite3 proteintracker.db < db.query_num_hits.2 > db.query_num_hits.out.2
		echo "select count(distinct ${table1}.bin_scaffold_gene)
		from $table1, $table2
		where ${table1}.bin_scaffold_gene =${table2}.bin_scaffold_gene;" > db.query_overlap
		sqlite3 proteintracker.db < db.query_overlap > db.query_overlap.out
		num1=`cat db.query_num_hits.out.1`
		num2=`cat db.query_num_hits.out.2`
		over=`cat db.query_overlap.out`
		echo "metagenome,hits"> db.summary_overlap.csv
		echo "${table1},${num1}">> db.summary_overlap.csv
		echo "${table2},${num2}" >>db.summary_overlap.csv
		echo "overlap,${over}" >>db.summary_overlap.csv
}

function load_data_in_database {
		 table=$1
		 file=$2
		 echo "drop table $table;" > db.load
		 echo "
			CREATE TABLE $table (
			bin_scaffold_gene text PRIMARY KEY,
			Subject_id text,
			Perc_identity text,
			query_length integer,
			alignment_length integer,
			mismatches integer,
			gap_openings integer,
			evalue real,
			bit_score real,
			taxid text,
			commonname text
			);
			" >> db.load
		 echo ".mode tabs" >> db.load
		 echo ".import $file $table" >> db.load
		 sqlite3 proteintracker.db < db.load		
         echo parameter1: table name= $table
         echo parameter2: input file= $file
		 echo rows inserted=
		 sqlite3 proteintracker.db "select count(*) from $table";
        }  

#run file
metagenome=$1
uniprot_file=$2
ghs_file=$3
load_data_in_database ${metagenome}_unitprot $uniprot_file
load_data_in_database ${metagenome}_ghs $ghs_file
query_num_overlap ${metagenome}_unitprot ${metagenome}_ghs
