#simple
subject=$1
: ${subject:='RCC email - process completed'}
echo sending completion mail subject: $1 1<&2
mail -s "$subject" cesarcardona@uchicago.edu < /dev/stdin
#with attachment
#mail -a mail.sh -s "Mail attachment" cesarcardona@uchicago.edu < /dev/null
