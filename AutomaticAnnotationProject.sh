#! /bin/sh

#########################################################
#Final project for Scientific Computing for Biologist

#Authors= Gabriel Vargas (jgvargas@uchicago.edu) & Cesar Cardona (cesarcardona@uchicago.edu)
#Department of Geophyscial Sciences
#Biophysical Sciences Department
#University of Chicago

#Created= March 19th 2015

#Objetive= This is script was develop as a pipeline for the automatic functional annotation of metagenomic
#assembled contigs against functional databases loading hits into sqllite database for further analyses
#########################################################

#Usage= bash Final_project_script_v1.1.sh
#This script is written to run on RCC Midway of University of Chicago


#Requiere modules

module load blastplus/2.2
module load python/2.7-2015q1

#Define input variables
#Input filename

metagenome=$1
infile=$2

#Break of fasta files into smaller pieces that can be run in parallel
#split_fasta.py is available at http://www.tc.umn.edu/~konox006/Code/SNPMeta/Split_FASTA.py


python split_fasta.py $infile  50

echo "split completed"

#Create a folder for the output files

mkdir Outputs/

###########################################################
### Open Reading Frame prediction in Prodigal 2.6.2  ######
#available at: https://github.com/hyattpd/prodigal/releases/
###########################################################


x1="*.fasta"

for i in $x1; do
echo "IN: $i"
if [ ! -s $i ]; then echo "$i is empty; skipping..."; continue; fi

    ./Prodigal-2.6.2/prodigal -p meta -i $i -o Outputs/$i.out -a Outputs/$i.aa -d Outputs/$i.genes -s Outputs/$i.gene_scores

echo "ORF prediction completed"

done

###########################################################
###Functional annotation of ORF using BLAST           #####
###Databases= Uniprot (http://www.uniprot.org/downloads) ##
###GHs= glycosil hydrolases compiled from RefSeq        ###
###########################################################


#In this step we use the fasta files containing the amino acid sequences, ".aa" files obtained from Prodigal
#to blast them against functional databases


#BLASTP vs Uniprot

x2="Outputs/*.fasta.aa"

for i in $x2; do
echo "IN: $i"
if [ ! -s $i ]; then echo "$i is empty; skipping..."; continue; fi

    blastp -query $i -db Uniprot/uniprot_sprot.fasta -out $i.blastp_uniprot_e5.out -evalue 0.00001 -outfmt "6 qseqid sseqid pident qlen length mismatch gapopen evalue bitscore staxids scomnames" -max_target_seqs 10 -num_threads 8

echo "FINISH blastp against UNIPROT"

done


#BLASTP vs GHs database

for i in $x2; do
echo "IN: $i"
if [ ! -s $i ]; then echo "$i is empty; skipping..."; continue; fi

blastp -query $i -db GHs_database/GHs_database.fasta -out $i.blastp_GHs_e5.out -evalue 0.00001 -outfmt "6 qseqid sseqid pident qlen length mismatch gapopen evalue bitscore staxids scomnames" -max_target_seqs 10 -num_threads 8

echo "FINISH blastp against GHs"

done

#################################################################
#Here we prepare the outputs to pushing them into a SQL database#
#################################################################

#The first step is to concatenate the outputs belongin to each original fasta file

cat Outputs/*.blastp_uniprot_e5.out > all_blastp_uniprot_e5.out
cat Outputs/*.blastp_GHs_e5.out > all_blastp_GHs_e5.out

#Second step is to parse BLAST output
#Here we are only keeping the best hit obtained for each query
#top5.pl was written by Sunit Jain from Geomicrobiology Lab at University of Michigan


perl top5.pl -b all_blastp_uniprot_e5.out -o all_blastp_uniprot_e5_TOPHIT.out -t 1
perl top5.pl -b all_blastp_GHs_e5.out -o all_blastp_GHs_e5_TOPHIT.out -t 1


echo "Top Hits completed"

#Organization of output files into directories

mkdir BLASTP_output

mv Outputs/*blastp_GHs_e5.out BLASTP_output
mv Outputs/*blastp_uniprot_e5.out BLASTP_output


rm *.fasta

############################################################################
#Build a SQL realtional database                                          ##
#The database is created in SQlite3                                       ##
############################################################################
bash Load_Query_Database.sh $metagenome all_blastp_uniprot_e5_TOPHIT.out all_blastp_GHs_e5_TOPHIT.out
Rscript Plot_Venn_Diagram.R
