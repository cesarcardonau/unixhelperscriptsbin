
#CCNote 3-26
##transform the format of the biom file
#OTUID air-settling-plate.1    air-settling-plate.3    air-settling-plate.4    air-settling-plate.5    air-settling-plate.8
#505587  22.0    0.0     4.0     12.0    15.0
#to a chrono per otu
#air-settling-plate_505587 0 22 0 0 4 12 0 0 15
#CCNote 5-6
#Now want to distinguish missings from 0
#air-settling-plate_505587  NA 22 NA  0 4 12 NA NA 15  

##CCNote 3-26
#possible reg expressions
#dont quite understand gensub, dont know why is working,but it is
#(.__[A-Za-z\[\]]+)
#regular expression is NOT working - build from ww.regexpal.com
#(.__[A-Za-z\[\]]+)(?!.*__[A-Za-z\[])
		#lastday=41;
gawk -v numdays="42" '
		#Begin loads variables
		BEGIN{
		firstday=0;
		lastday=numdays-1;
		numsurfaces=1;
		} 
		#Load headers names
		NR==2{
		/*DEBUG print $0,"\n";*/
		for(i=2;i<=NF-1;i++){ 
			split($i,time,".")
			surface[i]=time[1]
			indices[i]=time[2]
			/*DEBUG print indices[i]*/
		/*DEBUG	print i,surface[i]*/
		}} 
		NR>2 && NR<5{
		/*DEBUG print $0,"\n";*/
		stringout=surface[NR]"_"$1;
		for(i=2;i<=NF-1;i++){
			/*DEBUG print indices[i]*/
			datacnt[indices[i]]=$i;
		}
		for(i=firstday;i<=lastday;++i)
			if(length(datacnt[i])>0)
				stringout=stringout" "datacnt[i];
			else
				stringout=stringout" "NA;
		taxa=gensub(/.*(.__[A-Za-z\[\]]+).*/,"\\1","1",$NF)
		print stringout" "taxa
		}'
