file1=$1
file2=$2
mapfieldfile1=1
mapfieldfile2=1
#manually set what fields from the second file are you joining to the first file
awk  -v FS="\t" 'BEGIN {OFS="\t"} FNR==NR{hash1['"\$${mapfieldfile2}"']=$2 FS $3 FS $4;next}
                { print $0, hash1['"\$${mapfieldfile1}"']}' $file2 $file1
