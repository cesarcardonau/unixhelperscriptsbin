summary_file=shedd.IL.344.date.2159.summary
#confirm that +16 is still good, new_samples_names wc -l should be equal to numsamples in summary file
 tail -n +16 $summary_file | cut -d: -f1 | sort -u > new_sample_names 
cat new_sample_names | gawk 'BEGIN{OFS="\t";FS="."; print "#SampleID","Source2","DateSequence"}; {print $1"."$2,$1,$2}' > new_mapping.all

###manually remove unwanted sources or dates if any
cat new_mapping.all | gawk '$2 != "whale-skin" && $2 != "whale-stool" && $2 != "whale-nose" && $3 != "_none"{print $0}' > new_mapping.tmp
(head -n 1 new_mapping.all && tail -n +2 new_mapping.all | sort -k2,2 -k3,3n ) > new_mapping.before_cleaning
(head -n 1 new_mapping.tmp && tail -n +2 new_mapping.tmp | sort -k2,2 -k3,3n ) > new_mapping
rm new_mapping.all new_mapping.tmp

