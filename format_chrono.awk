###############################################
##transform the format of the biom file
#OTUID air-settling-plate.1    air-settling-plate.3    air-settling-plate.4    air-settling-plate.5    air-settling-plate.8
#CCNote 5-6
#Now want to distinguish missings from 0
#air-settling-plate_505587  NA 22 NA  0 4 12 NA NA 15  
###############################################
##CCNote 3-26
#possible reg expressions
#dont quite understand gensub, dont know why is working,but it is
#(.__[A-Za-z\[\]]+)
#regular expression is NOT working - build from ww.regexpal.com
#(.__[A-Za-z\[\]]+)(?!.*__[A-Za-z\[])
###############################################
if [ $# -lt 2 ]; then
    echo "Usage: `basename $0` firstday lastday < (stdin_input)"  1>&2
    exit 1
fi
#gawk -v lastday="41" -v firstday=0 '
gawk -v lastday="$2" -v firstday="$1" '
	#Begin loads variables
	BEGIN{
		FS="\t";OFS="\t";
		title="SITE_OTU"
		for(i=firstday;i<=lastday;i++)
			title=title" "i	
		print title" taxonomy"
	} 
	#Load headers names
	NR==2{
		/*DEBUG print $0,"\n";*/
		#for the first surface save it into a prior surface
		#and start the process of counting how many samples
		#we have for each surface type -- counting from 1
		surfaceid=1
		surfacefirstindex[1]=2
		split($2,firstsurface,".")
		priorsurface=firstsurface[1]
		surfacelist[1]=firstsurface[1]
		for(i=2;i<=NF-1;i++){ 
			split($i,surface,".")
			if(surface[1]!=priorsurface){
				priorsurface=surface[1]
				surfaceid=surfaceid+1
				surfacelist[surfaceid]=surface[1]
				surfacefirstindex[surfaceid]=i
			}
			cnt_samples[surfaceid]=cnt_samples[surfaceid]+1
		}
		#now for each surface save the time point of the sample
		#surface j and the i-th sample from it
		for(j=1;j<=surfaceid;j++) {
			for(i=surfacefirstindex[j];i<cnt_samples[j]+surfacefirstindex[j];i++){
				split($i,time,".")
				indices[j","i]=time[2]
			}
		} 
	}
	#now for all the actual data
	NR>2{
		#for each surface
		for(j=1;j<=surfaceid;j++) {
			split($NR,surface,".")
			strout=surfacelist[j]"_"$1;
			#first store the datacounts OTU count for the respective timepoint that
			#we determined earlier, this may have holes as this time skips occasionally
			#i is the counter of samples in the surface j
			for(i=surfacefirstindex[j];i<cnt_samples[j]+surfacefirstindex[j];i++){
				datacnt[j","indices[j","i]]=$i;
			}
			#now go in order for all the days that we have data and print the counts
			#for the samples that have available data, also report the missings
			for(i=firstday;i<=lastday;++i) {
				if(length(datacnt[j","i])>0)
					strout=strout" "datacnt[j","i];
				else
					strout=strout" NA";
			}
			#simplify the taxonomy string - see comments at the beginning
			taxa=gensub(/.*(.__[A-Za-z\[\]]+).*/,"\\1","1",$NF)
			print strout" "taxa;
		}
	}'
